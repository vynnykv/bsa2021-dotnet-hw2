﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private readonly Parking _parking;
        private readonly ILogService _logService;
        private readonly ITimerService _withdrawTimer;
        private readonly ITimerService _logTimer;

        public ParkingService(
            ITimerService withdrawTimer,
            ITimerService logTimer,
            ILogService logService
        )
        {
            if (withdrawTimer is null)
                throw new ArgumentNullException($"{nameof(withdrawTimer)} cannot be null");
            if (logTimer is null)
                throw new ArgumentNullException($"{nameof(logTimer)} cannot be null");
            if (logService is null)
                throw new ArgumentNullException($"{nameof(logService)} cannot be null");
            _parking = Parking.GetParking();
            _logService = logService;
            _withdrawTimer = withdrawTimer;
            _withdrawTimer.Interval = Settings.PaymentCancellationPeriod;
            _withdrawTimer.Elapsed += GetMoney;
            _logTimer = logTimer;
            _logTimer.Interval = Settings.LoggingPeriod;
            _logTimer.Elapsed += LogTransactions;
        }
        
        public void AddVehicle(Vehicle vehicle)
        {
            if (vehicle is null)
                throw new ArgumentNullException("Vehicle cannot be null");
            if (_parking.Vehicles.Count() >= Settings.ParkingCapacity)
                throw new InvalidOperationException("Parking is full");
            if (_parking.Vehicles.Any(v => v.Id == vehicle.Id))
                throw new ArgumentException("Vehicle with such an id exists");
            if (_parking.Vehicles.Count() == 0)
            {
                _withdrawTimer.Start();
                _logTimer.Start();
            }
            _parking.Vehicles.Add(vehicle);
        }

        public void Dispose()
        {
            _parking.Dispose();
            _withdrawTimer.Dispose();
            _logTimer.Dispose();
        }

        public decimal GetBalance()
        {
            return _parking.Balance;
        }

        public int GetCapacity()
        {
            return Settings.ParkingCapacity;
        }

        public int GetFreePlaces()
        {
            return Settings.ParkingCapacity - _parking.Vehicles.Count();
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return _parking.Transactions.ToArray();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new ReadOnlyCollection<Vehicle>(_parking.Vehicles.ToList());
        }

        public string ReadFromLog()
        {
            return _logService.Read();
        }

        public void RemoveVehicle(string vehicleId)
        {
            if (vehicleId is null)
                throw new ArgumentNullException("Id of vehicle cannot be null");
            var vehicle = _parking.Vehicles.FirstOrDefault(v => v.Id == vehicleId);
            if (vehicle == null)
                throw new ArgumentException("Vehicle with such an id is not found");
            if (vehicle.Balance < 0)
                throw new InvalidOperationException("Balance must be greater than zero");
            _parking.Vehicles.Remove(vehicle);
            if (_parking.Vehicles.Count() == 0)
            {
                _withdrawTimer.Stop();
                _logTimer.Stop();
            }
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (vehicleId is null)
                throw new ArgumentNullException("Id of vehicle cannot be null");
            var vehicle = _parking.Vehicles.FirstOrDefault(v => v.Id == vehicleId);
            if (vehicle == null)
                throw new ArgumentException("Vehicle with such an id is not found");
            if (sum < 0)
                throw new ArgumentException("Sum must be greater than zero");
            vehicle.Balance += sum;
        }

        private void GetMoney(object sender, ElapsedEventArgs e)
        {
            var vehicles = _parking.Vehicles.ToList();
            decimal profit;
            for(int i = 0; i<vehicles.Count; i++)
            {
                if (vehicles[i].Balance >= Settings.GetTariff(vehicles[i].VehicleType))
                {
                    profit = Settings.GetTariff(vehicles[i].VehicleType);
                    _parking.Balance += profit;
                    vehicles[i].Balance -= profit;
                    _parking.Transactions.Add(new TransactionInfo(vehicles[i].Id, profit, DateTime.Now));
                    continue;
                }
                if (vehicles[i].Balance >= 0)
                {
                    decimal LessZero = Settings.GetTariff(vehicles[i].VehicleType) - vehicles[i].Balance;
                    profit = vehicles[i].Balance + LessZero * Settings.FineCoefficient;
                    _parking.Balance += profit;
                    vehicles[i].Balance -= profit;
                    _parking.Transactions.Add(new TransactionInfo(vehicles[i].Id, profit, DateTime.Now));
                    continue;
                }
                profit = Settings.GetTariff(vehicles[i].VehicleType) * Settings.FineCoefficient;
                vehicles[i].Balance -= profit;
                _parking.Balance +=profit;
                _parking.Transactions.Add(new TransactionInfo(vehicles[i].Id, profit, DateTime.Now));
            }
        }

        private void LogTransactions(object sender, ElapsedEventArgs e)
        {
            string logTransactions = "";
            var transactions = _parking.Transactions.ToList();
            for(int i = 0; i < transactions.Count; i++)
            {
                logTransactions += $"VehicleId: {transactions[i].VehicleId} Sum: {transactions[i].Sum} TransactionTime: {transactions[i].TransactionTime:G} \n";
            }
            _logService.Write(logTransactions);
            _parking.Transactions.Clear();
        }
    }
}