﻿using CoolParking.BL.Interfaces;
using System;
using System.IO;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        private readonly string _logFilePath;
        public string LogPath => _logFilePath;

        public LogService(string logFilePath)
        {
            if (logFilePath == null)
                throw new ArgumentNullException("logFilePath cannot be null", nameof(logFilePath));
            _logFilePath = logFilePath;
        }

        public string Read()
        {
            if (!File.Exists(_logFilePath))
                throw new InvalidOperationException("File is not found");
            using (var streamReader = new StreamReader(_logFilePath))
            {
                var data = streamReader.ReadToEnd();
                return data;
            }
        }

        public void Write(string logInfo)
        {
            if (logInfo == null)
                throw new ArgumentNullException("logInfo cannot be null", nameof(logInfo));
            using (var streamWriter = new StreamWriter(_logFilePath, true))
            {
                streamWriter.WriteLine(logInfo);
            }
        }
    }
}