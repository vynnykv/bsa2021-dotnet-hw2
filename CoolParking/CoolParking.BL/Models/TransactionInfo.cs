﻿using System;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public string VehicleId { get; private set; }
        public decimal Sum { get; set; }
        public DateTime TransactionTime { get; private set; }
        public TransactionInfo(string vehicleId, decimal sum, DateTime transactionTime)
        {
            #region checking conditions
            if (vehicleId is null)
                throw new ArgumentNullException("Id cannot be null", nameof(vehicleId));
            if (sum < 0)
                throw new ArgumentException("Sum must be greater than zero", nameof(sum));
            #endregion
            VehicleId = vehicleId;
            Sum = sum;
            TransactionTime = transactionTime;
        }
    }
}