﻿using System;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        private static readonly Random random = new Random();
        public string Id { get; private set; }
        public VehicleType VehicleType { get; private set; }
        public decimal Balance { get; internal set; }
        public Vehicle(
            string id,
            VehicleType vehicleType,
            decimal balance)
        {
            #region checking conditions
            if (id is null)
                throw new ArgumentNullException("Id cannot be null", nameof(id));
            if (!IsValidId(id))
                throw new ArgumentException("Id must be in ХХ-YYYY-XX format", nameof(id));
            if (balance < 0)
                throw new ArgumentException("Balance must be greater than zero", nameof(balance));
            #endregion
            Id = id;
            VehicleType = vehicleType;
            Balance = balance;
        }
        public static string GenerateRandomRegistrationPlateNumber()
        {
            var identifier = new char[10];

            for (var i = 0; i < identifier.Length; i++)
            {
                if (i < 2 || i > 7)
                    identifier[i] = (char)random.Next('A', 'Z' + 1);
                else
                    identifier[i] = (char)random.Next('0', '9' + 1);
            }
            identifier[2] = identifier[7] = '-';
            return new string(identifier);
        }
        private bool IsValidId(string id)
        {
            var regex = new Regex(@"\p{Lu}{2}-[0-9]{4}-\p{Lu}{2}");
            return regex.IsMatch(id);
        }
    }
}
