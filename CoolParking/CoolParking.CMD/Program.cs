﻿using CoolParking.BL.Models;
using CoolParking.BL.Services;
using System.Linq;
using System;
using System.Threading;

namespace CoolParking.CMD
{
    class Program
    {
        private static ParkingService _parkingService = new ParkingService(new TimerService(),new TimerService(), new LogService(Settings.LogPath));
        static void Main()
        {
            DisplayMainMenu();
            MainMenu();
        }

        static void DisplayMainMenu()
        {
            Console.WriteLine("Welcome to Cool Parking!\n\n" +
                "Enter a number what have to do:\n" +
                "1. Display the current balance of the parking.\n" +
                "2. Display the amount of money earned for the current period.\n" +
                "3. Display the number of free parking spaces.\n" +
                "4. Display all parking Transactions for the current period.\n" +
                "5. Display the transaction history.\n" +
                "6. Display the list of transports in the parking lot.\n" +
                "7. Put the transport in the parking lot.\n" +
                "8. Remove the transport from the parking.\n" +
                "9. Top up the balance of a transport.\n" +
                "10. Clear console.\n" +
                "11. Exit program.\n");
        }

        static void MainMenu()
        {
            Console.Write("Your choice: ");
            string choice = Console.ReadLine();
            try
            {
                switch (choice)
                {
                    case "1":
                        Console.WriteLine($"Current balance is {_parkingService.GetBalance()}");
                        break;
                    case "2":
                        Console.WriteLine($"The amount of money earned for the current period: {_parkingService.GetLastParkingTransactions().Sum(t => t.Sum)}");
                        break;
                    case "3":
                        Console.WriteLine($"Amount free place of parking is {_parkingService.GetFreePlaces()} out of {_parkingService.GetCapacity()}");
                        break;
                    case "4":
                        {
                            var transactions = _parkingService.GetLastParkingTransactions();
                            Console.WriteLine("The history of transactions for current period:");
                            foreach (var transaction in transactions)
                                Console.WriteLine($"VehicleId: {transaction.VehicleId} Sum: {transaction.Sum} TransactionTime: {transaction.TransactionTime:T}");
                            break;
                        }
                    case "5":
                        {
                            Console.WriteLine("Transactions history:");
                            var history = _parkingService.ReadFromLog();
                            Console.WriteLine(history);
                            break;
                        }
                    case "6":
                        {
                            Console.WriteLine("The transports in the parking lot:");
                            var transports = _parkingService.GetVehicles();
                            foreach (var transport in transports)
                                Console.WriteLine($"{transport.Id}: {transport.VehicleType.ToString()} Balance: {transport.Balance}");
                            break;
                        }
                    case "7":
                        {
                        Console.WriteLine("Enter transport id, type of transport and amount of money:");
                        Console.Write("Vehicle id: ");
                        var id = Console.ReadLine();
                        int type;
                        Console.Write("Type of transport(0 - \"PassengerCar\", 1 - \"Truck\", 2 - \"Bus\", 3 - \"Motorcycle\"): ");
                        while (!int.TryParse(Console.ReadLine(), out type) || (type > 3 || type < 0))
                        {
                            Console.WriteLine("Type of transport must be 0-3");
                            Console.Write("Type of transport(0 - \"PassengerCar\", 1 - \"Truck\", 2 - \"Bus\", 3 - \"Motorcycle\"): ");
                        }
                        Console.Write("Your money: ");
                        decimal money;
                        decimal.TryParse(Console.ReadLine(), out money);
                        _parkingService.AddVehicle(new Vehicle(id, (VehicleType)type, money));
                        Console.WriteLine("Thanks for parking your transport");
                        break;
                    }
                    case "8":
                    {
                        Console.WriteLine("Enter the id of transport:");
                        string id = Console.ReadLine();
                        _parkingService.RemoveVehicle(id);
                        Console.WriteLine("Have a nice day!");
                        break;
                    }
                    case "9":
                    {
                        Console.Write("Enter the id of transport: ");
                        string id = Console.ReadLine();
                        Console.Write("Sum: ");
                        decimal money;
                        decimal.TryParse(Console.ReadLine(), out money);
                        _parkingService.TopUpVehicle(id, money);
                        Console.WriteLine($"Thanks");
                        break; 
                    }

                    case "10":
                        Console.Clear();
                        break;

                    case "11":
                        Environment.Exit(0);
                        break;
                    default:
                        Console.WriteLine("Entered incorrect number, try again");
                        break;
                }
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
            }

            catch (InvalidOperationException ex)
            {
                Console.WriteLine(ex.Message);
            }

            catch (FormatException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Thread.Sleep(1000);
                Console.WriteLine();
                DisplayMainMenu();
                MainMenu();
            }
        }
    }
}
